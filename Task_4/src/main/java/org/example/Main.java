package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        int desat = 123;
        String vosem = "";

        while (desat > 0) {
            int yaddash = desat % 8;
            vosem = yaddash + vosem;
            desat = desat / 8;
        }

        System.out.println(vosem);

    }
}