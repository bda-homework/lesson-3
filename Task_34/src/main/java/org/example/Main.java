package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    static class LoginProgram {
        private static final String myUsername = "Murad";
        private static final String myPassword = "m0123456789";
        private static final int myAttempts = 3;

        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            int say = 0;
            boolean loggedIn = false;

            while (!loggedIn && say < myAttempts) {
                System.out.print("Username - i daxil edin: ");
                String username = scanner.nextLine();

                System.out.print("Password - i daxil edin: ");
                String password = scanner.nextLine();

                if (!username.equals(myUsername)) {
                    System.out.println("Username duzgun daxil edilmeyib.");
                    say++;
                } else if (!password.equals(myPassword)) {
                    System.out.println("Password duzgun daxil edilmeyib.");
                    say++;
                } else {
                    loggedIn = true;
                    System.out.println("Xosh geldiniz, " + username + "!");
                }
            }

            if (!loggedIn) {
                System.out.println("Maksimum sehv password limitini kecmisiniz. Sag olun!");
            }
        }
    }
}