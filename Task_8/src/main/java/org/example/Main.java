package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner1 = new Scanner(System.in);
        System.out.print("Bayırda hava istidir? (hə - true, yox - false): ");
        boolean shert1 = scanner1.nextBoolean();

        if (shert1) {
            System.out.println("Onda dənizə getmək olar");
        } else {
            System.out.println("Təssüf ki, dənizə gedə bilməyəcəksiz");
        }

    }
}