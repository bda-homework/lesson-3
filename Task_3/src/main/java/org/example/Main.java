package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        String bin = "101101";
        int dex = 0;

        for (int i = 0; i < bin.length(); i++) {
            char sym = bin.charAt(i);
            int reqem = Character.getNumericValue(sym);
            int stepen = bin.length() - i - 1;
            dex += reqem * Math.pow(2, stepen);
        }

        System.out.println(dex);

    }
}