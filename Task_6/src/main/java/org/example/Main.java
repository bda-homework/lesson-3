package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("1ci rəqəmi daxil edin: ");
        int num1 = scanner.nextInt();

        System.out.print("2ci rəqəmi daxil edin: ");
        int num2 = scanner.nextInt();

        System.out.print("istədiyinizi seçin (true - vurulma, false - toplama): ");
        boolean shert = scanner.nextBoolean();

        if (shert) {
            int result = num1 * num2;
            System.out.println(num1 + " * " + num2 + " = " + result);
        } else {
            int result = num1 + num2;
            System.out.println(num1 + " + " + num2 + " = " + result);
        }

    }
}