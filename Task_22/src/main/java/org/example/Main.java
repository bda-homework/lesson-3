package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner8 = new Scanner(System.in);

        System.out.println("1ci Reqem daxil edin: ");
        int num1 = scanner8.nextInt();

        System.out.println("2ci reqem daxil edin: ");
        int num2 = scanner8.nextInt();

        System.out.println("True ve ya false sechin: ");
        boolean operator = scanner8.nextBoolean();

        if(operator) {
            System.out.println(num1 * num2);
        } else {
            System.out.println(num1 + num2);
        }

    }
}