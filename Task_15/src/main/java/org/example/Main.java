package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner2 = new Scanner(System.in);

        System.out.println("Birinci reqemi daxil edin: ");
        double reqem1 = scanner2.nextDouble();

        System.out.println("Ikinci reqemi daxil edin: ");
        double reqem2 = scanner2.nextDouble();

        System.out.println("Operatoru daxil edin (+, -, *, /): ");
        char operator = scanner2.next().charAt(0);

        double netice = 0.0;

        switch(operator) {
            case '+':
                netice = reqem1 + reqem2;
                break;
            case '-':
                netice = reqem1 - reqem2;
                break;
            case '*':
                netice = reqem1 * reqem2;
                break;
            case '/':
                netice = reqem1 / reqem2;
                break;
            default:
                System.out.println("Duzgun operator qeyd edin");
                break;
        }

        System.out.println(netice);

    }
}