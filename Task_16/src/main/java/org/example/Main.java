package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner3 = new Scanner(System.in);

        System.out.println("Reqem daxil edin: ");
        int daxilOlunanReqem = scanner3.nextInt();

        if (daxilOlunanReqem % 2 == 0 && daxilOlunanReqem > 50) {
            System.out.println("Ugurlu emeliyyat");
        } else {
            System.out.println("Ugursuz emeliyyat");
        }

    }
}