package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        //Birinci hisse - hefte gunleri
        int dayOfWeek = 8;
        String dayName;

        switch (dayOfWeek) {
            case 1:
                dayName = "Birinci";
                break;
            case 2:
                dayName = "Ikinci";
                break;
            case 3:
                dayName = "Uchuncu";
                break;
            case 4:
                dayName = "Dorduncu";
                break;
            case 5:
                dayName = "Beshinci";
                break;
            case 6:
                dayName = "Altinci";
                break;
            case 7:
                dayName = "Yeddinci";
                break;
            default:
                dayName = "Heftede cemi 7 gun var";
                break;
        }

        System.out.println(dayName);

        //Ikinci hisse - aylar
        int numberOfMonth = 8;
        String monthName;

        switch (numberOfMonth) {
            case 1:
                monthName = "Yanvar";
                break;
            case 2:
                monthName = "Fevral";
                break;
            case 3:
                monthName = "Mart";
                break;
            case 4:
                monthName = "Aprel";
                break;
            case 5:
                monthName = "May";
                break;
            case 6:
                monthName = "Iyun";
                break;
            case 7:
                monthName = "Iyul";
                break;
            case 8:
                monthName = "Avqust";
                break;
            case 9:
                monthName = "Sentyabr";
                break;
            case 10:
                monthName = "Oktyabr";
                break;
            case 11:
                monthName = "Noyabr";
                break;
            case 12:
                monthName = "Dekabr";
                break;
            default:
                monthName = "Ilde cemi 12 ay var";
                break;
        }

        System.out.println(monthName);

    }
}