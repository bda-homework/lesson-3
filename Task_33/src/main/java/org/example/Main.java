package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        printSubstring("Hello world", 3, 7);
    }
    public static void printSubstring(String a, int begin, int end) {
        for (int i = begin; i < end; i++) {
            System.out.println(a.charAt(i));
        }
    }

}