package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        int dec = 45;
        String hex = "";

        while (dec > 0) {
            int son = dec % 16;
            if (son < 10) {
                hex = son + hex;
            } else {
                if (son == 10) {
                    hex = "A" + hex;
                } else if (son == 11) {
                    hex = "B" + hex;
                } else if (son == 12) {
                    hex = "C" + hex;
                } else if (son == 13) {
                    hex = "D" + hex;
                } else if (son == 14) {
                    hex = "E" + hex;
                } else if (son == 15) {
                    hex = "F" + hex;
                }
            }
            dec = dec / 16;
        }

        System.out.println(hex); // выведет 7D

    }
}