package org.example;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner4 = new Scanner(System.in);

        System.out.println("Reqem daxil edin: ");
        int daxilEdilmishReqem = scanner4.nextInt();

        if (daxilEdilmishReqem % 2 == 0 || daxilEdilmishReqem > 100) {
            System.out.println("Ugurlu emeliyyat");
        } else {
            System.out.println("Ugursuz emeliyyat");
        }

    }
}