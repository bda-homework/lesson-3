package org.example;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        boolean result = containsString("word", "world", "or" );
        System.out.println(result);
    }
    public static boolean containsString(String a, String b, String c) {
        return a.contains(c) && b.contains(c);
    }

}